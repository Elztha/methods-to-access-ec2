# Skillset-The different way to connect to EC2

Amazon Elastic Compute Cloud (Amazon EC2) provides scalable computing capacity in the Amazon Web Services (AWS) cloud. Using Amazon EC2 eliminates your need to invest in hardware up front, so you can develop and deploy applications faster. You can use Amazon EC2 to launch as many or as few virtual servers as you need, configure security and networking, and manage storage. Amazon EC2 enables you to scale up or down to handle changes in requirements or spikes in popularity, reducing your need to forecast traffic.

<h4>How do we access EC2 ?</h4> 
AWS supports some ways to connect your servers(EC2). 
In this tutorial, we will try use three kind of methods for access your EC2. There are by SSH Connection, EC2 Instance Connect, and System Manager and deep dive in EC2 Instance Connect and System Manager – Session Manager.

## Scenario & Architect
In this tutorial, we will try to access our server with three different way

### 1. SSH Connection 
SSH Connection is the most traditional way to access the server. With local terminal/ CMD and the keypair.pem file in local, makes you are the only one can access the server. Because that condition, the person who access the server is just you.

<p align="center">
    <img src="./Images/A.jpg" alt="1.jpg" width="70%" height="70%">
</p>


### 2. EC2 Instance Connect
Amazon EC2 Instance Connect is a simple and secure way to connect to your instances using Secure Shell (SSH). 

With EC2 Instance Connect, you can control SSH access to your instances using AWS Identity and Access Management (IAM) policies as well as audit connection requests with AWS CloudTrail events. In addition, you can leverage your existing SSH keys or further enhance your security posture by generating one-time use SSH keys each time an authorized user connects. Instance Connect works with any SSH client, or you can easily connect to your instances from a new browser-based SSH experience in the EC2 console.


<p align="center">
    <img src="./Images/B.jpg" alt="1.jpg" width="70%" height="70%">
</p>


### 3. System Manager-Session Manager
Session Manager offers these benefits:
* Centralized access control to instances using IAM policies
* No open inbound ports and no need to manage bastion hosts or SSH keys
* One-click access to instances from the console and CLI
* Port forwarding
* Cross-platform support for both Windows and Linux
* Logging and auditing session activity


<p align="center">
    <img src="./Images/C.jpg" alt="1.jpg" width="70%" height="70%">
</p>


## Prerequisites

> Prepare an AWS account. </br>

> Make sure your are in US East (N. Virginia), which short name is us-east-1.

## Lab tutorial
### Create Role

1. Log in into AWS Account and go to **IAM Console**.

2. Choose ***Roles*** on the left.

3. Select **Create Role**

4. Under Select type of trusted entity, choose **AWS service**.

5. Under Choose the service that this role will use, in the full list of services, choose **EC2** 

6. Under Select your use case, choose EC2, and the choose **Next:Permissions**.

7. In the list of policies, select the 
    - **AmazonSSMManagedInstanceCore**

8. Choose Next: **Tags**.

9. Choose Next: **Review**.

10. For Role name, enter a name for your new role, such as **AmazonSSMRoleForInstancesQuickSetup**

11. Choose Create role.


### Create Your VPC

1.  In the **AWS Management Console**, on the **service menu**, click **VPC**.

<p align="center">
    <img src="./Images/1-1.jpg" alt="1.jpg" width="70%" height="70%">
</p>


2. In the navigation pane, click **Your VPCs**.

3. Click **Create VPC**, enter the following details:
    * Name tag: 
        * Yourname-EC2Access
    * IPv4 CIDR block: 
        * 10.0.0.0/16

<p align="center">
    <img src="./Images/1-2.jpg" alt="1.jpg" width="70%" height="70%">
</p>

4. Click **Yes, Create**. and then Select your VPC, and than click right, **Edit DNS hostnames**, tick **enable**, and **save**.

5. In the navigation pane, click **Internet Gateways**.

<p align="center">
    <img src="./Images/1-5.jpg" alt="1.jpg" width="70%" height="70%">
</p>


6. Click **Create Internet Gateway**, enter Name tag: **IGW-YournameEC2Access**, then click **Create**.

<p align="center">
    <img src="./Images/1-6.jpg" alt="1.jpg" width="70%" height="70%">
</p>

7. Choose **IGW-YournameEC2Access**, click **Actions**, click **Attach to VPC**, and then choose **Yourname-EC2Access** you created, click **Yes, Attach**.

<p align="center">
    <img src="./Images/1-7.jpg" alt="1.jpg" width="70%" height="70%">
</p>

<p align="center">
    <img src="./Images/1-8.jpg" alt="1.jpg" width="70%" height="70%">
</p>

8. In the navigation pane, click **Subnets**.

<p align="center">
    <img src="./Images/1-3.jpg" alt="1.jpg" width="70%" height="70%">
</p>

9. Click **Create Subnet**, enter the following details:
    * Name tag: 
        * Yourname-EC2Access-subnet1
    * VPC: 
        * Yourname-EC2Access
    * Availability Zone: 
        * us-east-1a
    * IPv4 CIDR block: 
        * 10.0.0.0/24

<p align="center">
    <img src="./Images/1-4.jpg" alt="1.jpg" width="70%" height="70%">
</p>

10. In the navigation pane, click **Route Tables**.

11. Search your VPC route tables

<p align="center">
    <img src="./Images/1-10.jpg" alt="1.jpg" width="70%" height="70%">
</p>

12. Click **Routes** tab in the lower pane of the console, click **Edit**, click **Add another route**, enter the following details:
    * Destination: 
        * 0.0.0.0/0
    * Target: 
        * choose **igw-xxxxxxxx** that you created earlier

<p align="center">
    <img src="./Images/1-11.jpg" alt="1.jpg" width="70%" height="70%">
</p>

<p align="center">
    <img src="./Images/1-12.jpg" alt="1.jpg" width="70%" height="70%">
</p>

13. Click **Save**.

14. In the navigation pane, click **security groups**.

<p align="center">
    <img src="./Images/1-13.jpg" alt="1.jpg" width="70%" height="70%">
</p>

15. Click **Create security groups**, enter the following details:
    * Security group name: 
        * Yourname-EC2SG
    * Description: 
        * For Tutorial EC2
    * VPC: 
        * Yourname-EC2Access

<p align="center">
    <img src="./Images/1-14.jpg" alt="1.jpg" width="70%" height="70%">
</p>

16. Select the **Yourname-EC2SG** you just created, and then click **Inbound Rules** tab in the lower pane of the console, click **Edit Rules**, Inbound rules to allow the following ports:
    * All- traffic
        > source : anywhere

<p align="center">
    <img src="./Images/1-15.jpg" alt="1.jpg" width="70%" height="70%">
</p>

<p align="center">
    <img src="./Images/1-16.jpg" alt="1.jpg" width="70%" height="70%">
</p>

### Launch an instance

1. On the **service** menu,click **EC2**.


2. Click **Launch Instance**.

<p align="center">
    <img src="./Images/1.jpg" alt="1.jpg" width="70%" height="70%">
</p>

3. In the navigation pane, choose **Quick Start**, in the row for the second **Amazon Linux 2 AMI (HVM), SSD Volume Type - ami-0323c3dd2da7fb37d (64-bit x86) / ami-0ce2e5b7d27317779 (64-bit Arm)**  click **Select**.

<p align="center">
    <img src="./Images/2.jpg" alt="1.jpg" width="70%" height="70%">
</p>

4. On **Step2: Choose a Instance Type** page,make sure **t2.micro** is selected and click **Next: Configure Instance Details**.

<p align="center">
    <img src="./Images/3.jpg" alt="1.jpg" width="70%" height="70%">
</p>

5. On **Step3: Configure Instance Details** page, enter the following and leave all other values with their default:
    * Network: 
        * Yourname-EC2Access
    * Subnet: 
        * Yourname-EC2Access-subnet1
    * Auto-assign Public IP:
        * Enable
    * IAM role : 
        * AmazonSSMRoleForInstancesQuickSetup

<p align="center">
    <img src="./Images/121.jpg" alt="1.jpg" width="70%" height="70%">
</p>

6. Click **Next: Add Storage**, leave all values with their default.

<p align="center">
    <img src="./Images/5.jpg" alt="1.jpg" width="70%" height="70%">
</p>

7. Click **Next: Tag Instance**.

8. Click **Next: Configure Security Group**, Select **Yourname-EC2SG**.

<p align="center">
    <img src="./Images/6.jpg" alt="1.jpg" width="70%" height="70%">
</p>

9. Click **Review and Launch**.

<p align="center">
    <img src="./Images/7.jpg" alt="1.jpg" width="70%" height="70%">
</p>

10. Review the instance information and click **Launch**.

11. Click **Create a new key pair**, enter the **Key pair name (ex.Yournameec2access)**, click **Download Key Pair**. Put your key in Desktop

<p align="center">
    <img src="./Images/8.jpg" alt="1.jpg" width="70%" height="70%">
</p>

12. Click **Launch Instances**.

13. Scroll down and click **View Instances**.

14. Wait for shows 2/2 checks passed in the **Status Checks** column.
> This will take 3-5 minutes. Use the refresh icon at the top right to check for updates.

15. Click __connect__

## SSH Connection 

<p align="center">
    <img src="./Images/A.jpg" alt="1.jpg" width="70%" height="70%">
</p>

<p align="center">
    <img src="./Images/9-1.jpg" alt="9-1.jpg" width="70%" height="70%">
</p>


1. Open CMD or Terminal

2. Type __cd Desktop__

3. Copy and paste the **Example** (ssh -i /path/my-key-pair.pem ec2-user@ec2-xx-xx-xx-xx.compute-1.amazonaws.com)

<p align="center">
    <img src="./Images/10-1.jpg" alt="10-1.jpg" width="70%" height="70%">
</p>

4. Enter __yes__. 

<p align="center">
    <img src="./Images/11-1.jpg" alt="11-1.jpg" width="70%" height="70%">
</p>

5. You are connect to your instance ! 

## EC2 Instance Connect

<p align="center">
    <img src="./Images/B.jpg" alt="1.jpg" width="70%" height="70%">
</p>

<p align="center">
    <img src="./Images/9-2.jpg" alt="9-2.jpg" width="70%" height="70%">
</p>

1. Select the __EC2 Instance Connect (browser-based SSH connection)__, than __connect__

<p align="center">
    <img src="./Images/10-2.jpg" alt="9-2.jpg" width="70%" height="70%">
</p>

2. You are connect to your instance !

## Session Manager 

<p align="center">
    <img src="./Images/C.jpg" alt="1.jpg" width="70%" height="70%">
</p>

<p align="center">
    <img src="./Images/9.jpg" alt="9-2.jpg" width="70%" height="70%">
</p>

1. Select the __Session Manager__, than __connect__

<p align="center">
    <img src="./Images/13-3.jpg" alt="9-2.jpg" width="70%" height="70%">
</p>

2. You are connect to your instance !


### Challenge 
After we try to access the server EC2 with 3 different kind of methods, 
1. Which method is the most secure ?
2. What is the reason you choose that method to access the server EC2 ?
3. Which method is the most convenience to use ?

### Clean Up
To delete the AWS resources, perform the tasks below in order:
1. Terminate the Instances
2. Delete VPC
3. Delete IAM Role


## Conclusion
Congratulations! We now have learned how to:
* How to deploy the EC2.
* Connect EC2 by SSH Connection.
* Connect EC2 by EC2 Instance Connect.
* Connect EC2 by Session Manager.

